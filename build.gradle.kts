/*
 * Copyright (c) 2019 Nimbly Games, LLC all rights reserved
 */

import org.gradle.api.tasks.wrapper.Wrapper.DistributionType.ALL

tasks.named<Wrapper>("wrapper") {
   distributionType = ALL
   gradleVersion = "6.4.1"
}
