# Software project workflow (Work in progress)

![Karl Sabo](https://en.gravatar.com/userimage/176551206/18bcdab3fb1caee1eaacd9723726480d.png "Karl Sabo")
 
 by Karl Sabo

Creating software projects that scale beyond a few thousand lines of code and a handful of developers is difficult. This article will outline some general guidelines that will start a project off on the right foot for scalability, maintainability, and quality.

The first step is to split all code into logical groupings based on what type of problems the code solves. For example, input/output code would be placed in an IO project, cryptographic code would be in a Crypto project, etc. By splitting code into projects it makes it easier for a developer to find code that might already solve a problem, encourages designing code that is reusable, and keeps compile times fast. With a multi-project setup you also get the benefit of each project having its own continuous integration system, issue tracker, wiki, etc. All of these systems help to organically share knowledge and improve the codebase. They are also easy to setup using modern code hosting sites such as [GitLab](https://gitlab.com), [GitHub](https://github.com/), and [BitBucket](https://bitbucket.org/).

With the code logically separated into projects the next step is to assign a version to each project. Versioning helps with tracking changes, makes it easier for dependent projects to reliably build against, it can make builds of dependent projects faster, and it allows for more aggressive refactoring. A great starting point for versioning project is [Semantic Versioning](https://semver.org/), it is the most commonly used versioning scheme. Semantic Versioning consists of three numbers, MAJOR.MINOR.PATCH, the semantics to increasing any of the numbers is well understood and dependent projects will know what to expect when they see a version change.

Similarly important to the high level splitting and versioning of code is managing all the changes that occur in the codebase. There are several popular workflows and this article will focus on [trunk based development](https://trunkbaseddevelopment.com/) as a great way to manage a code base. Git is currently the most widely used version control system for software development and its a great tool for using a trunk based development workflow. Using the git master branch as the trunk and creating development, release, and graveyard branches helps maintain a manageable codebase. Performing all changes on a development branch help protect the integrity of master and open the door for the powerful git hosting tools with [merge requests](https://www.quora.com/In-GitHub-why-is-the-pull-request-a-request-to-merge-code-into-another-branch-often-Master-called-pull-request-In-GitLab-its-called-%E2%80%9Cmerge-request-which-seems-more-straightforward) and automated continuous integration checks. In addition to powerful merging, git hosting tools can automatically update issues when they detect an issue number in a git commit. 

# Splitting code into logical projects and versioning

All code should be split into logical projects, just like you would if you were creating a library for publishing publicly. For example, input output code could be in an IO project, command line argument parsing code could be in a CLI project, cryptographic code could be in a Crypto project, etc. Just like when working on libraries or projects for public consumption, all internal projects should be versioned, and the binaries of those versions should be published to an artifact server for easy consumption.

In addition to splitting code into logical projects. Each project should be versioned. Versioning projects has several benefits, it makes fulls builds process faster, it allows for more aggressive refactoring by using the major version number to indicate API changes, and it prevents issues from cascading and breaking the builds of all dependent projects. One downside to splitting an internal code base into versioned projects is losing the ability to refactor code in a base project and have it refactor all dependent projects. Depending on your build system, it's possible to get the best of both worlds, versioned projects and refactoring across all projects. In the Gradle build system this capability is referred to as [Composite builds](https://docs.gradle.org/current/userguide/composite_builds.html).

When picking how to version projects, start with [Semantic Versioning](https://semver.org/) and decide if it satisfies your requirements. The core of semantic versioning is a three part versioning system, MAJOR.MINOR.PATCH, e.g., 2.1.5. The highest order number (major) is incremented when API changes are made that would break backwards compatibility. The second order number (minor) is incremented when new functionality is added but won't break backwards compatibility and the lowest order number (patch) is incremented for bug fixes that won't break backwards compatibility.

When a base project has its version increased, the build system for dependent projects should complain loudly that they need to update to the latest version. It's also reasonable for the master branch build of dependent projects to fail when it falls too far behind on a version of a dependency. When a new version of a project is released, it's important to have a good changelog to make it easier for dependent projects to track down any issues that arise during the upgrade process.

In the simplest of cases, each change to the code base should be tracked with an issue in an issue tracking system. Most issue tracking systems allow for tagging what version of the software the issue was completed in and this is a great basis for a changelog.

As a quick example, lets assume some code made sense to be split into two projects, IOProject and CoolApp. Here's a simple example of upgrading IOProject from version 1.0 to 2.0 and how to upgrade CoolApp to use the new version of IOProject.

![multi project versioning](./multi-project-trim.png "multi project versioning")
 
When IOProject increments its version, then dependent project CoolApp should create a dev branch off of master named dev/upgradeIOProjectTo2.0, increment the dependency version to 2.0 of IOProject and follow the guidelines for merging the dev branch back into master (See [Development branches](#development-branches)).

If the version upgrade to IOProject 2.0 fails the CoolApp build, the task is now to determine if the error is in the CoolApp or the IOProject. If the error is with the CoolApp project then it's simply a matter of making fix commits to the dev/upgradeIOProjectTo2.0 branch, until it builds and then creating a merge request as outlined in "[Development branches](#development-branches)". The more interesting case is where the error lies in the IOProject, an issue should be created for the IOProject discussing the problem and then a new dev branch is created off the release2.0 branch and the issue can be fixed and appropriate unit tests can be added. Once IOProject release 2.0.1 is available, CoolApp development branch "dev/upgradeIOProjectTo2.0" can be picked up and new commits made to upgrade to IOProject 2.0.1. The development branch "dev/upgradeIOProjectTo2.0" is a good history of the upgrade process to the new major version and serves as a record of the issues encountered during the upgrade to IOProject 2.0.

In the case where IOProject 2.0 had issues discovered by updating CoolApp to the new version, the issues created and the new changelog for IOProject 2.0.1 create useful information for any other project that might depend upon IOProject. For example, an HTTPProject that depends on IOProject went to upgrade after CoolApp and discovered the same bug, the HTTPProject team will now discover the issue already exists and they can provide more information and potentially help resolve the issue faster. This feedback loop of sharing information by updating issues, and tying git commits to issues creates a rich history and documentation for all projects that utilize IOProject. This rich documentation will also help future maintenance because there is a reason tied to every change and a test case for it.

# Using git to support trunk based development, versioning, and changelogs

Git and modern hosting tools built around git provide an incredibly powerful system that facilitate fast iteration, stable master branch, solid releases, and rich changelogs. 

All the powerful features in git can lead to astonishingly complex repositories. In-order to combat this, a simple but powerful development approach needs to be adhered to. One such approach is [trunk based development](https://trunkbaseddevelopment.com/) where git "master" branch is the trunk. In this model, developers collaborate on code in a single branch and resist pressure to create other long-lived [Development branches](#development-branches). *"They therefore avoid merge hell, do not break the build, and live happily ever after."*

The key benefit of trunk based development is reducing the distance between developers. Reducing the distance improves collaboration and communication. With everyone working as closely as possible to master, features, and bug fixes are organically discovered by everyone and the whole team can benefit immediately. When combined with merge request code reviews for development branches, knowledge sharing is even greater.

High level overview of the git branching model that will be discussed below:
![Workflow graph](./git-workflow-graph.png "Workflow graph")

## Development branches

The first place to start with an empty codebase is adding new features. Both features and bug fixes are added by creating a new branch off master.

Development branches should be named with the prefix dev/ followed by a short yet descriptive name in camel case and optionally a hyphen followed by the issue tracker number, dev/&lt;shortDescriptiveName-\#\#\#\#\>, e.g.:
-  dev/someFeature
-  dev/fixingPomGeneration-123

When writing code, perform "git commit" regularly

-   At a high level it makes sense to perform a commit when the code compiles and a logical piece of functionality has been added or a bug has been fixed
    -   Commit when a unit of work is completed
    -   There are changes that might need to be undone
    -   You can always amend a commit if it hasn't been pushed
-   Committing regularly helps with writing a history (the commit message) of why a change was made

When work is complete on the the feature or bug fix and you think the code is ready to be merged back into master, it's time to run a full build and have the code reviewed. Depending on your git hosting platform, some or all of these steps might be automated. The following is a general guideline of steps to perform before merging your development branch back into master.

-   Perform a full build to ensure all tests pass
    - Unless automated by your version control management and CI system     
-   Create a ["merge request"](https://www.quora.com/In-GitHub-why-is-the-pull-request-a-request-to-merge-code-into-another-branch-often-Master-called-pull-request-In-GitLab-its-called-%E2%80%9Cmerge-request-which-seems-more-straightforward) or email 1 to 2 peers to perform a code review.
    - [GitLab merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html)
    - [GitHub pull request](https://help.github.com/en/articles/creating-a-pull-request)
    - [BitBucket pull request](https://www.atlassian.com/git/tutorials/making-a-pull-request)
- Merge the development branch into master
    - Unless automated by your version control management system

### Guidelines for completing units of work

Because of the wording "commit" and most tools providing a diff view when creating a commit, it can seem like the code going into a commit should be fully reviewed and pass a full-build. However, because of useful commit-time tools ([IntelliJ Before Commit](https://www.jetbrains.com/help/idea/commit-changes-dialog.html#before_commit), and [git hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks)) it can be useful to think of commits as more fluid and to take advantage of [git commit --amend](https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History). It's important to keep in mind that local repository commits can be amended with no risk of breaking the git history of others. However, any commits that have been pushed to a shared repository should not be changed.

Here are some useful flows for completing a unit of work:

    do
      modify code
      build and test modified code
    until(unit of work is complete)
    git commit
    
    do
      diff review commit (make changes as needed, such as removing debug output)
      full build w/ all tests (make changes as needed to fix the build)
    until(code looks good, builds, and passes tests)
    git commit --amend
    git push

When implementing a feature that comprises a lot of units of work it can make sense to commit these units of work without constantly running a full-build. Here is a more fleshed out flow:

    for(units of work in logical piece of functionality)
      do
        modify code
        build and test modified code
      until(unit of work is complete) 
      git commit
      do
        diff review commit (make changes as needed, such as removing debug output)
      until(code looks good and passes unit tests)

    # logical functionality is complete, perform a full-build before pushing
    # if fixes are needed, create a new commit and amend it as needed until the build works
    do
      full build w/ all tests (make changes as needed to fix the build)
    until(code looks good, builds, and passes tests)
    git commit (if fixes were necessary to have the build pass)
    git push

### Push regularly

Perform a "git push" to origin/dev/&lt;branchName&gt; regularly (at least every 3 days)
-   This ensures all work is backed up
-   Work can be picked up by other developers when needed

### Long-lived development branches

For long-lived feature branches regularly merge or rebase master into the development branch
-   Best practice is to merge master into the development branch on a weekly basis (at least every 10 business days)
    -   Rebase can be used to keep merge commits out of the git history
        -   [When to merge or rebase](https://medium.com/@porteneuve/getting-solid-at-git-rebase-vs-merge-4fa1a48c53aa)
            -   "As its name implies, merge performs a merge, a fusion. We want to move the current branch ahead so it incorporates the work of another branch. The real question you should ask yourself is this: "what does this other branch represent?" Is it just a local, temporary branch, that I had just created out of precaution, in order for master to remain clean in the meantime? If so, it is not only useless but downright counter-productive for this branch to remain visible in the history graph, as an identifiable "railroad switch."\"
            -   "As its name suggests, rebase exists to change the "base" of a branch, which means its origin commit. It replays a series of commits on top of a new base. This is mostly needed when local work (a series of commits) is deemed to start from an obsolete base. This could happen several times a day, when you try to push local commits to a remote only to be denied because the tracking branch (say origin/master) is stale: since it last sync'd with our remote, someone pushed updates to it, so that pushing our own code path would overwrite that previously-sent, parallel work. This is not nice to our collaborators, so push gives us the boot.\"
        -   More tips for rebasing: [how to not dread rebases when managing long lived feature branches](https://seesparkbox.com/foundry/how_to_not_dread_rebases_when_managing_long_lived_feature_branches)

Figures with graphical representations of merging versus rebasing when the commits end up on the master branch:
![git development branch graph](./graph-dev-branch.png "git development branch graph")


![git development branch merged into master](./graph-merge.png "git development branch merged into master")
![git development branch rebased into master](./graph-dev-rebase.png "git development branch rebased into master")

### Graveyard development branches

If work is stopped for a long period of time on a development branch, it should be renamed to graveyard/&lt;branchName&gt;.

-   git checkout dev/&lt;branchName&gt;
-   git branch -m graveyard/&lt;branchName&gt;
-   git push origin --delete dev/&lt;branchName&gt;
-   git push origin -u graveyard/&lt;branchName&gt;

## Release branches

Releases branches should be created off of the master branch. The naming scheme for release branches is "release/&lt;version&gt;". E.g., "release/3.0.0". All patch releases will use the MAJOR.MINOR release branch. For example, release 3.0.1 commits will be made to the release/3.0.0 branch and a tag created that represents the commits which make up "3.0.1". The idea being that all patch releases should be adopted downstream negating the need for a branch on every patch release.

Once a release branch is created, only bug fix commits should be made to the branch. This ensures there is no feature creep for the release and helps to keep the release stable. E.g., no API changes should be made to patches on a release branch, even if they are backwards compatible. Backward compatible API features should be put into a new MAJOR.MINOR release branch.

As bug fixes are applied, the release branch is merged back into master regularly. To create a fix for the release branch, branch off of release/&lt;version&gt; and follow the steps in "[Development branches](#development-branches)", replacing master with release/&lt;version&gt;

When a fix is made to the master branch that is needed in the release branch, a cherry pick from master into the release branch is okay. Depending on pre-existing commits and changes to master, a cherry pick can pull in code that won't work on the release branch. When cherry picking commits from master into release, follow the same branching guidelines as when making a bug fix. That is to say, create a branch off of the release branch, cherry pick the commits into the new branch and follow the standard CI/CD and merge request guidelines from "[Development branches](#development-branches)".

Creating a branch in addition to a tag for releases makes it easier to create patch releases with further bug fixes. It also makes it easier to check out the latest version of the release as you simply check out the release branch instead of finding the latest patch version tag.

## Commit messages

1.  Separate subject from body with a blank line
2.  Limit the subject line to 80 characters (denoted by a grey line in IntelliJ)
    * 80 characters is not a hard limit, just a guideline. Limiting subject lines length helps ensure that they are readable, and helps make a concise thought out explanation about what's going in this commit.
    * Note: Some tools such as GitHub truncate subjects to 72 characters
3.  Capitalize the subject line
4.  Do not end the subject line with a period
5.  Use the imperative mood in the subject line \[[*](https://gist.github.com/robertpainsi/b632364184e70900af4ab688decf6f53)\]
    * Imperative mood just means "spoken or written as if giving a command or instruction". A few examples:
        * Refactor subsystem X for readability
        * Update getting started documentation
        * Remove deprecated methods
        * Release version 1.0.0
        * A properly formed Git commit subject line should always be able to complete the following sentence:
        * If applied, this commit will &lt;your subject line here&gt;
        * If applied, this commit will refactor subsystem X for readability
        * If applied, this commit will update getting started documentation
6.  Use the body to explain what and why vs. how \[[*](https://gist.github.com/robertpainsi/b632364184e70900af4ab688decf6f53)\]
    * Describe why a change is being made.
    * How does it address the issue?
    * What effects does the patch have?
    * Do not assume the reviewer understands what the original problem was.
    * Do not assume the code is self-evident/self-documenting.
    * Read the commit message to see if it hints at improved code structure.
    * The first commit line is the most important.
    * Describe any limitations of the current code.
    * Do not include patch set-specific comments.
7.  If the commit refers to an issue, add this information to the commit message subject or body. This is a very important step for adding more information to issues and creating richer changelogs.
    * Resolves: \#123
    * See also: \#456, \#789
    * Fixes: \#123, \#124

# Conclusion

Starting with good practices are paramount to creating maintainable, scalable, projects. For software projects this starts with placing the code that solves similar problems into the same project, making the code easier to find and more reusable. After code is divided into logical projects the next step to ensure reusability and maintainability is to version each project. Versioning projects makes it easier for other projects to use and speeds up continuous integration. Combining good issue tracking practices with versioning also creates clear-cut changelogs.

In addition, git and its many hosting services help manage projects. Creating development branches, committing units of work, pushing regularly and ultimately code reviewing all merge requests into the master branch help ensure that the master branch has high quality, compiling and unit test passing code at all times. Git commit messages also play an important role in code reviews by providing a what and why explanation, and linking to issues in an issue tracking system. Using git branches to create MAJOR.MINOR releases makes it easy to create patches to a release of a project, and create built artifacts that can be uploaded to an artifact server.

With these foundational steps, large multi-project code bases will scale to millions of lines of code and hundreds of developers. These steps also keep code quality higher, more maintainable, and easier to use for all projects. 

# References, citations and useful links 

## workflow

-   [trunkbaseddevelopment.com/](https://trunkbaseddevelopment.com/)
-   [barro.github.io/2016/02/a-succesful-git-branching-model-considered-harmful/](https://barro.github.io/2016/02/a-succesful-git-branching-model-considered-harmful/)
-   [about.gitlab.com/2014/09/29/gitlab-flow/](https://about.gitlab.com/2014/09/29/gitlab-flow/)
-   [docs.gitlab.com/ee/workflow/gitlab\_flow.html](https://docs.gitlab.com/ee/workflow/gitlab_flow.html)
-   [nvie.com/posts/a-successful-git-branching-model/](https://nvie.com/posts/a-successful-git-branching-model/)
-   [team-coder.com/from-git-flow-to-trunk-based-development/](https://team-coder.com/from-git-flow-to-trunk-based-development/)
-   [www.toptal.com/software/trunk-based-development-git-flow](https://www.toptal.com/software/trunk-based-development-git-flow)
-   [guides.github.com/introduction/flow/](https://guides.github.com/introduction/flow/)
-   [4-branching-workflows-for-git-30d0aaee7bf](https://medium.com/@patrickporto/4-branching-workflows-for-git-30d0aaee7bf)
-   [www.endoflineblog.com/gitflow-considered-harmful](https://www.endoflineblog.com/gitflow-considered-harmful)
-   [seesparkbox.com/foundry/how\_to\_not\_dread\_rebases\_when\_managing\_long\_lived\_feature\_branches](https://seesparkbox.com/foundry/how_to_not_dread_rebases_when_managing_long_lived_feature_branches)

## when to commit

-   <https://jasonmccreary.me/articles/when-to-make-git-commit/>

## rebase or merge

-   <https://medium.com/@porteneuve/getting-solid-at-git-rebase-vs-merge-4fa1a48c53aa>
-   <https://seesparkbox.com/foundry/how_to_not_dread_rebases_when_managing_long_lived_feature_branches>

## commit messages

-   [chris.beams.io/posts/git-commit/](https://chris.beams.io/posts/git-commit/)
-   [gist.github.com/robertpainsi/b632364184e70900af4ab688decf6f53](https://gist.github.com/robertpainsi/b632364184e70900af4ab688decf6f53)
-   [tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html](https://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html)
-   [wiki.openstack.org/wiki/GitCommitMessages](https://wiki.openstack.org/wiki/GitCommitMessages)

## amend a commit

-   <https://blog.codeminer42.com/crazy-developer-in-git-commit-amend-7367cfde16ac>

## versioning software

-  <https://blog.codeship.com/best-practices-when-versioning-a-release/>
-  <https://semver.org/>

Copyright 2019 Karl Sabo 

![Creative commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png "Creative commons")

[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

Please provide a link back to <https://gitlab.com/karl.sabo/git-workflow/blob/master/README.md> when creating derivative works. 